using CSV
using DataFrames
using Dates
using Plots
#plotly()
#gr(yscale=:log10)
#gui()


BASE_URL = "https://raw.githubusercontent.com/datasets/covid-19/master/data/"
CSV_data=["countries-aggregated.csv","key-countries-pivoted.csv", "reference.csv",
     "time-series-19-covid-combined.csv","us_confirmed.csv","us_deaths.csv",
     "worldwide-aggregated.csv" ]

storePath="COVID-19/"

countries = sort(["Liechtenstein","Switzerland","Austria",  "Germany", "United Kingdom", "US", "China"])
file=CSV_data[1]
filename=download(BASE_URL*file,storePath*file);

countries = sort(["Liechtenstein","Switzerland","Austria",  "Germany", "United Kingdom", "US","China" ,"Sweden"])

df = CSV.File(filename,normalizenames=true) |> DataFrame;

#cd(raw"D:\Development\Julia\Makie")

#df=df[in.(df.Country, [countries]),: ]
df=groupby(df,:Country);
#countryKeys = keys(covid)
#country = Dict()

struct CountryInfected
  name::String
  Date::Vector{Date}
  Confirmed::Vector{Int}
  Recovered::Vector{Int}
  Deaths::Vector{Int}
end

function getCountryData(country::String,df::GroupedDataFrame)
    countryKeys = keys(df)
    index = 1
    data = nothing
    for text in countryKeys
        if country in text
            data = df[index]
        end
        index +=1
    end
    if data != nothing
        return select!(data, Not(:Country))
    else
        error("not fould")
    end
    #return CountryInvected(country,data.Date,data.Confirmed,data.Recoverd,data.Deaths)
end

function getCountry(country::String,df::GroupedDataFrame)
    countryKeys = keys(df)
    index = 1
    data = nothing
    for text in countryKeys
        if country in text
            data = df[index]
        end
        index +=1
    end
    if data != nothing
        return CountryInfected(country,data.Date,data.Confirmed,data.Recovered,data.Deaths)
    else
        error("Country ''$country' not found!")
    end
end


function getRelative(c::CountryInfected)
    data =c.Confirmed -c.Recovered
    data = float(data) ./ maximum(data)
    end

countries =  map(countries) do country
        getCountry(country,df)
    end ;

c=countries[1]
data = c.Confirmed;
#data =c.Confirmed -c.Recovered
data = float(data) ./ maximum(data)
NOzeroData=data.>0;
plt=plot(c.Date[NOzeroData], data[NOzeroData], label=c.name,leg=:bottomright, yscale=:log10)
#plot(c.Date[NOzeroData], data[NOzeroData], xticks=c.Date[1:5:end], xrotation=45, leg=:topleft,
#    label=c.name, m=:o,
#    yscale=:log)
for c in countries[2:end]
    data = c.Confirmed
    #data =c.Confirmed -c.Recovered
    data = float(data) ./ maximum(data)
    NOzeroData=data.>0;
    plot!(plt,c.Date[NOzeroData],data[NOzeroData],label=c.name)
end
display(plt)

using Flux
using Flux: @epochs

Austria = getCountry("Austria",df)
t=Austria.Date
maxT=length(t)
t_steps=range(0,stop=1,length=maxT)
target_data = getRelative(Austria)

t_steps=reduce(hcat,t_steps)
target_data=reduce(hcat,target_data)


#create the model
σk = 0.2
μk =  0.0

#CModel = t-> p[3] .* exp.(-p[4] .* ((t .- p[2] ) ./ p[1]).^2)

CModel= Chain(t-> p[3] .* exp.(-p[4] .* ((t .- p[2] ) ./ p[1]).^2),Dense(1,5,σ),LSTM(5,5),Dense(5,1))

plot(t_steps,target_data)

##
## Generate data that will be used to train the network
## The data is the number of rabbits/wolves at set times
##

              # vector of 101 data points

scatter!(t_steps, target_data, color = [1], label = "data")


##
## Parameter optimisation
##

# Loss function is the total squared error
# (the number of points is constant - mean is not necessary)
loss_function = function()

    # Calculate squared error
  return sum(abs2, CModel(t_steps')' - target_data)
end


# Callback function to observe training
iter = 0
callback = function ()
  global iter += 1
  if iter % 10 == 1
    # Plot the training data
    @show(loss_function())
    scatter(t_steps,  target_data, color = [1], label = " data")
    # Use `remake` to re-create the original `prob0` with different current parameters `p`
    display(plot!(t_steps, CModel(t_steps')', labels = " model", color = 2))
  end
end


# Vector of new parameters different from p0
p = [0.08, 0.65, 1, 0.5]
#p = [1.0, 0.7, 1.0, 1.0]
ODEparams = Flux.params(CModel)
scatter(t_steps,target_data)
plot!(t_steps, CModel(t_steps')', labels = " model", color = 2)
# Display the ODE with the initial parameter values.
callback()


# The parameter `loss_data` in `train!` (below) contains a list of data that are provided
# to the loss function. `train!`` works by iterating over that list. In our case, the
# calculation of the loss function does not require any additional data.
# Therefore `loss_data` is filled with a whole bunch of nothing.
loss_data = Iterators.repeated((), 100000000)

# ADAM is a great default optimiser
optimiser = Flux.ADAM(0.1)
optimiser = Flux.Descent()
#optimiser = ADAM(0.01, (0.99, 0.999))

callback_stoper = function ()
    #display(loss_function())
    loss_function() < 0.8 && Flux.stop()
end

# train! is a function that hides some of the complexity of the library
Flux.train!(loss_function, ODEparams, loss_data, optimiser; cb = callback_stoper)
display("finished")
