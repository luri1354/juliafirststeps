using ModelingToolkit
using DifferentialEquations

#using OrdinaryDiffEq
using Plots
using BenchmarkTools

@parameters t Vs R Tsw RD
@variables vIN(t) vR(t) vD(t) i(t)
@derivatives dt'~t


p=[Vs=>10.0 ,R=>1.0,Tsw=>0.1,RD=>1e6];
#p=[10.0, 0.1, 1.0 ,1.0,0.1];
# initial condition for the system
u0=[vIN=>0.0,vR=>0.0,vD=>0.0,i=>0.0];
u0=[vIN=>0.0, i=>0];

less_nasty(x) = 1-1/(1 + exp(1e2 * (1 - x)))

eqs = [ dt(vIN) ~ Vs*cos(t/Tsw)/Tsw,
#        0 ~ vR-i*R ,
#        0 ~ vIN-vD-vR,
       0 ~ i-vIN/(R+RD)
       ]

sys = ODESystem(eqs,t,[vIN,i],[Vs,R, Tsw,RD])


function f1!(integrator)
    integrator.p[4]=10e-3;
end
function choice1(u,t,integrator)
    u[1]<0.7
end
cb1=ContinuousCallback(choice1, f1!)

function f2!(integrator)
    integrator.p[4]=1e6;
end
function choice2(u,t,integrator)
    u[1]>0.7
end
cb2=ContinuousCallback(choice2, f2!)


cb=CallbackSet(cb1,cb2)


tspan = (0.0,20.0)

prob4 = ODEProblem(sys,u0,tspan,p) #;jac=true,sparse=true
sol = solve(prob4,Rodas4(),reltol=1e-6,abstol=1e-6,callback=cb)
sol = solve(prob4,Rodas4(),reltol=1e-6,abstol=1e-6)
#plot(sol,layout=(2,1), title=" I / V")
plot(sol,layout=(2,1),tspan=(0, 1),vars=([vIN,i]))
