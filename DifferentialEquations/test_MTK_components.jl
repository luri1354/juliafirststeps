using ModelingToolkit
using DifferentialEquations

using Plots

@derivatives D'~t
@parameters t

#resistor
@parameters R
@variables i(t) v(t)


eps_Resistor = [0 ~ i-v/R]
resistor1=ODESystem(eps_Resistor,name=:R1)

#inductor
@parameters L
@variables i(t) v(t)

eps_Inductor = [D(i) ~ v/L]
inductor1=ODESystem(eps_Inductor,name=:L1)

#capacitor
@parameters C
@variables i(t) v(t)

eps_Capacitor= [D(v) ~ i/C]
capacitor1=ODESystem(eps_Capacitor,name=:C1)

#supply
@parameters Vs, Ri
@variables i(t) v(t)

eps_Supply= [ 0 ~ v-Vs ]
supply1=ODESystem(eps_Supply,name=:V1)

#switch
@parameters Tsw d Ron Roff Rsw
@variables i(t) vp(t) vn(t)

eps_SW= [ 0 ~ i - (vp-vn)/Rsw ]
sw1=ODESystem(eps_SS,name=:SW1)

#halfbridge
@parameters Tsw d g Ron Roff RswH RswL
@variables i(t) vp(t) vn(t) vSW(t)

eps_HB= [  0  ~ (vp-vn)* RswL/(RswH + RswL)-vSW]
halfbridge1=ODESystem(eps_SS,name=:HB1)

function swHon!(integrator)
    integrator.p[7]= 0.0; RswH = Ron
    integrator.p[6] = 10e6; RswL = Roff
end
function time_swHon(integrator)
    Tsw = integrator.p[5]; Tsw
    Δt1 = Tsw*g
    #    @show integrator.t
    return integrator.t < Δt1 ? Δt1 : integrator.t+Tsw
end
cbSW1=IterativeCallback(time_swHon, swHon!)

function swLon!(integrator)
    integrator.p[7]=10e6; RswH = Roff;
    integrator.p[6] = 0.0; RswL = Ron;
end

function time_swLon(integrator)
    Tsw = integrator.p[5]; Tsw
    Δt2 = Tsw*(d+g)
    #@show integrator.t
    return integrator.t < Δt2 ? Δt2 : integrator.t+Tsw
end
cbSW2=IterativeCallback(timtime_swLone_choice, swLon!)

#diode



@parameters t Vs L C R Tsw RswH RswL
@variables iL(t) vOut(t) vSW(t)
@derivatives dt'~t


lorenz1 = ODESystem(eqs,name=:lorenz1)
lorenz2 = ODESystem(eqs,name=:lorenz2)

@variables α
@parameters γ
connections = [0 ~ lorenz1.x + lorenz2.y + sin(α*γ)]
connected = ODESystem(connections,[α],[γ],systems=[lorenz1,lorenz2])
