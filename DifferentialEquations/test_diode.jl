using DifferentialEquations

#using OrdinaryDiffEq
using Plots

less_nasty(x) = 1-1/(1 + exp(1e3 * (1 - x)))

function diodeTest(du,u,p,t)
 #u[1] = p[1]*sin(t/p[2])
du[1]=p[1]*cos(t/p[2])/p[2]
 u[2] =u[1]/(1+less_nasty(u[1])
end

p=[10.0,0.1]
u0 = [0.0;0.0]
tspan = (0.0,10.0)
prob = ODEProblem(diodeTest,u0,tspan,p)
sol = solve(prob)
plot(sol,layout=(2,1))
