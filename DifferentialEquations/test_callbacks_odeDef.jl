using DifferentialEquations, ParameterizedFunctions
using Plots
using BenchmarkTools
 ##vSW = (Vsw * switch)[1]

toScalar(a) = a[1];

buck = @ode_def Buck_def begin
  di =  (toScalar(Vsw * switch)-v)/L #vl=L*di/dt => di/dt = VL/L
  dv = (i-v/R)/C  #Ic = C dV/dt =>
end Vsw L C R sw switch

function f1!(integrator)
    integrator.p[6]=[1,0];
end
function time_choice1(integrator)
    Tsw = integrator.p[5][1]
    Δt1 = Tsw*integrator.p[5][3]
    return integrator.t < Δt1 ? Δt1 : integrator.t+Tsw
end
cb1=IterativeCallback(time_choice1, f1!)


function f2!(integrator)
    integrator.p[6]=[0,1];
end
function time_choice2(integrator)
    Tsw = integrator.p[5][1]
    Δt2 = Tsw*integrator.p[5][2]+Tsw*integrator.p[5][3]
    return integrator.t < Δt2 ? Δt2 : integrator.t+Tsw
end
cb2=IterativeCallback(time_choice2, f2!)

cb_2=CallbackSet(cb1,cb2)

Vin=10.0
C=1.0
L=0.1
R=1.0

Tsw = 1/10.0; #10Hz
d=0.4;
g=0.0;
ΔI = Vin*d/L*(d*Tsw)
ΔI = Vin*d/L*(d*Tsw)

p=[[Vin 0.0], L, C,R,(Tsw,d,g,Vin,0.0),[1, 0]];
# initial condition for the system
#u0=[d*Vin/R, Vin];
u0=[0.0,0.0];

tspan = (0.0,10.0)
prob2 = ODEProblem(buck,u0,tspan,p)
#sol2 = solve(prob2,callback=cb_2)
#0.002834 seconds (44.26 k allocations: 1.229 MiB)
#plot(sol2,layout=(3,1))
