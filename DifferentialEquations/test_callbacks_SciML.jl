using ModelingToolkit
using DifferentialEquations

using Plots

@parameters t Vs L C R Tsw RswH RswL
@variables iL(t) vOut(t) vSW(t)
@derivatives dt'~t


p=[Vs=>10.0, L=>0.1, C=>1.0 ,R=>1.0,Tsw=>0.1, RswL=>0.0, RswH=>10e6];
# initial condition for the system
u0=[iL=>0.0,vOut=>0.0,vSW=>0.0];

eqs = [dt(iL) ~ ( vSW - vOut)/L ,
       dt(vOut) ~ (iL-vOut/R)/C,
     0  ~ Vs* RswL/(RswH + RswL)-vSW]
      #vSW  ~ Vs* RswL/(RswH + RswL)]

#eqs = [dt(iL) ~ ( Vs* RswL/(RswH + RswL) - vOut)/L ,
#             dt(vOut) ~ (iL-vOut/R)/C]

sys = ODESystem(eqs,t,[iL,vOut,vSW],[Vs, L, C,R, Tsw, RswL, RswH])
ode_f = ODEFunction(sys)

function f1!(integrator)
    integrator.p[7]= 0.0;
    integrator.p[6] = 10e6;
    #@show integrator.p[1]
end
function time_choice1(integrator)
    Tsw = integrator.p[5]
    Δt1 = 0.0
    #    @show integrator.t
    return integrator.t < Δt1 ? Δt1 : integrator.t+Tsw
end
cb1=IterativeCallback(time_choice1, f1!)

function f2!(integrator)
    integrator.p[7]=10e6;
    integrator.p[6] = 0.0;
    #@show integrator.p[1]
end
#Δt2 = d*Tsw;
#cb2=PeriodicCallback(f2, Δt2; initial_affect = false)


function time_choice(integrator)
    Tsw = integrator.p[5]
    Δt2 = Tsw*0.4+0
    #@show integrator.t
    return integrator.t < Δt2 ? Δt2 : integrator.t+Tsw
end

cb2=IterativeCallback(time_choice, f2!)

cb_3=CallbackSet(cb1,cb2)
# [Vs  ,   L,    C,R,Tsw]
p=[10.0, 0.1, 1.0,1,0.1, 10e6, 0.0];
u0=[0.0,0.0,0.0]
tspan = (0.0,10.0)
#prob = ODEProblem(Buck,u0,tspan,p)
#sol = solve(prob,callback=cb)
prob3 = ODEProblem(ode_f,u0,tspan,p) #;jac=true,sparse=true
prob3 = ODEProblem(sys,u0,tspan,p)
sol = solve(prob3,Rodas5(),callback=cb_3,reltol=1e-9,abstol=1e-9,dtmax=1e-9)
@benchmark sol = solve(prob3,Rodas5(),reltol=1e-3,abstol=1e-3,dtmax=1e-3,callback=cb_3)
@benchmark sol = solve(prob3,Rosenbrock23(),force_dtmin=true,callback=cb_3) #best
@benchmark sol = solve(prob3,RadauIIA5(),force_dtmin=true,callback=cb_3)
@benchmark sol = solve(prob3,Rodas4(),reltol=1e-3,abstol=1e-3,dtmax=1e-3,callback=cb_3)
@benchmark sol = solve(prob3,callback=cb_3)
sol = solve(prob3,callback=cb_3)
sol = solve(prob3,RadauIIA5(),force_dtmin=true,callback=cb_3,save_everystep=true)
plot(sol,layout=(3,1),tspan=(9.8, 10),vars=([iL,vOut,vSW]),title="Rosenbrock23")
plot(sol,layout=(3,1),tspan=(9.8, 10),vars=([iL,vOut,vSW]),title="RadauIIA5")
