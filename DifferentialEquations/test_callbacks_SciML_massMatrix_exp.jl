using ModelingToolkit
using DifferentialEquations

#using OrdinaryDiffEq
using Plots
using BenchmarkTools

@parameters t Vs L C R Tsw
@variables vOut(t) vSW(t) iL(t)
@derivatives dt'~t


p=[Vs=>10.0, L=>0.1, C=>1.0 ,R=>1.0,Tsw=>0.1];
#p=[10.0, 0.1, 1.0 ,1.0,0.1];
# initial condition for the system
u0=[vOut=>0.0,vSW=>0.0,iL=>0.0];

eqs = [dt(vOut) ~ iL/C,
       0 ~ (0+Vs*sin(t/Tsw))-vSW-iL*R-vOut,
       0 ~ (tanh(vSW-3)/2.0+0.5)*iL
       ]


sys = ODESystem(eqs,t,[vOut,vSW,iL],[Vs, L, C,R, Tsw])

tspan = (0.0,10.0)
#prob = ODEProblem(Buck,u0,tspan,p)
#sol = solve(prob,callback=cb)
prob4 = ODEProblem(sys,u0,tspan,p) #;jac=true,sparse=true
#sol = solve(prob,Rodas5(),reltol=1e-8,abstol=1e-8,callback=cb)
sol = solve(prob4,Rodas5(),reltol=1e-6,abstol=1e-6)
#plot(sol,layout=(2,1), title=" I / V")
plot(sol,layout=(3,1),tspan=(0, 10),vars=([vOut,iL,vSW]),title="SciML ModelingToolkit")
