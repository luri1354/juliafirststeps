using ModelingToolkit
using DifferentialEquations
#using OrdinaryDiffEq
using Plots
using BenchmarkTools

@parameters t Vs L C R Tsw
@variables iL(t) vOut(t) vSW(t) vL(t)
@derivatives dt'~t


p=[Vs=>10.0, L=>0.1, C=>1.0 ,R=>1.0,Tsw=>0.1];
#p=[10.0, 0.1, 1.0 ,1.0,0.1];
# initial condition for the system
u0=[iL=>0.0,vOut=>0.0,vSW=>0.0];

eqs = [dt(iL) ~ (vSW-vOut)/L ,
       dt(vOut) ~ (iL-vOut/R)/C,
       0 ~ (Vs+Vs*sin(t/Tsw))-vSW]

sys = ODESystem(eqs,t,[iL,vOut,vSW,vL],[Vs, L, C,R, Tsw])


function f1!(integrator)
    integrator.p[1]= 10.0;
    #@show integrator.p[1]
end
function time_choice1(integrator)
    Tsw = integrator.p[5]
    Δt1 = 0.0
    #    @show integrator.t
    return integrator.t < Δt1 ? Δt1 : integrator.t+Tsw
end
cb1=IterativeCallback(time_choice1, f1!)

function f2!(integrator)
    integrator.p[1]=0.0;
    #@show integrator.p[1]
end
#Δt2 = d*Tsw;
#cb2=PeriodicCallback(f2, Δt2; initial_affect = false)


function time_choice(integrator)
    Tsw = integrator.p[5]
    Δt2 = Tsw*0.4+0
    #@show integrator.t
    return integrator.t < Δt2 ? Δt2 : integrator.t+Tsw
end

cb2=IterativeCallback(time_choice, f2!)

cb_4=CallbackSet(cb1,cb2)

tspan = (0.0,10.0)
#prob = ODEProblem(Buck,u0,tspan,p)
#sol = solve(prob,callback=cb)
prob4 = ODEProblem(sys,u0,tspan,p) #;jac=true,sparse=true
#sol = solve(prob,Rodas5(),reltol=1e-8,abstol=1e-8,callback=cb)
sol = solve(prob4,Rodas5(),reltol=1e-8,abstol=1e-8)
#plot(sol,layout=(2,1), title=" I / V")
plot(sol,layout=(2,1),tspan=(0, 10),vars=([iL,vSW]),title="SciML ModelingToolkit")
