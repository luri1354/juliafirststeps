using DifferentialEquations
using Plots

function Buck(du,u,p,t)
    Vsw,L,C,R,SW = p
    iL,vOut,vSW = u
    du[1] =  (vSW-vOut)/L #vl=L*di/dt => di/dt = VL/L
    du[2] = (iL-vOut/R)/C  #Ic = C dV/dt =>
    #u[3] = vSW
end

function f1!(integrator)
    integrator.u[3]=integrator.p[5][4];
end
function time_choice1(integrator)
    Tsw = integrator.p[5][1]
    Δt1 = Tsw*integrator.p[5][3]
    return integrator.t < Δt1 ? Δt1 : integrator.t+Tsw
end
cb1=IterativeCallback(time_choice1, f1!)


function f2!(integrator)
    integrator.u[3]=integrator.p[5][5];
end
function time_choice(integrator)
    Tsw = integrator.p[5][1]
    Δt2 = Tsw*integrator.p[5][2]+Tsw*integrator.p[5][3]
    return integrator.t < Δt2 ? Δt2 : integrator.t+Tsw
end
cb2=IterativeCallback(time_choice, f2!)

cb=CallbackSet(cb1,cb2)

Vin=10.0
C=1.0
L=0.1
R=1.0

Tsw = 1/10.0; #10Hz
d=0.4;
g=0.5;
ΔI = Vin*d/L*(d*Tsw)

p=[Vin, L, C,R,(Tsw,d,g,Vin,0.0)];
# initial condition for the system
#u0=[d*Vin/R, Vin];
u0=[0.0,0.0,0.0];

tspan = (0.0,10.0)
prob = ODEProblem(Buck,u0,tspan,p)
@time sol = solve(prob,callback=cb)
#0.002834 seconds (44.26 k allocations: 1.229 MiB)
plot(sol,layout=(3,1), title=" I / V")
