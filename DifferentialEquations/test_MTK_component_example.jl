using ModelingToolkit
using DifferentialEquations

using Plots

@parameters t σ ρ β
@variables x(t) y(t) z(t)
@derivatives D'~t

eqs = [D(x) ~ σ*(y-x),
       D(y) ~ x*(ρ-z)-y,
       0 ~ x + y + β*z]

lorenz1 = ODESystem(eqs,t,[x,y,z],[σ,ρ,β],name=:lorenz)
lorenz2 = ODESystem(eqs,t,[x,y,z],[σ,ρ,β],name=:lorenz2)

@parameters α
@variables a(t)
connnectedeqs = [D(a) ~ a*lorenz1.x]

connected1 = ODESystem(connnectedeqs,t,[a],[α],systems=[lorenz1,lorenz2],name=:connected1)

u0map = [lorenz1.x => 1.0,
         lorenz2.x => 2.0,
         lorenz1.y => 3.0,
         lorenz2.y => 4.0,
         lorenz1.z => 5.0,
         lorenz2.z => 6.0,
         a => 7.0]
#using StaticArray
#SA
parammap = [lorenz1.σ => 1.0,
              lorenz1.ρ => 2.0,
              lorenz1.β => 3.0,
              lorenz2.σ => 4.0,
              lorenz2.ρ => 5.0,
              lorenz2.β => 6.0,
              α => 7.0]

prob = ODEProblem(connected1,u0map,(0.0,100.0),parammap,jac=true)
