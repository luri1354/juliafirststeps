using DifferentialEquations
using ModelingToolkit
#using OrdinaryDiffEq
using Plots


@parameters t Vin L C R
@variables iL(t) vOut(t)
@derivatives dt'~t


p=[Vin=>10.0, L=>0.1, C=>1.0 ,R=>1.0];
# initial condition for the system
u0=[iL=>0.0,vOut=>0.0];

eqs = [dt(iL) ~ (Vin-vOut)/L ,
       dt(vOut) ~ (iL-vOut/R)/C ]

sys = ODESystem(eqs)
#sys = ode_order_lowering(sys)

tspan = (0.0,20.0)
#prob = ODEProblem(Buck,u0,tspan,p)
#sol = solve(prob,callback=cb)
prob = ODEProblem(sys,u0,tspan,p)
sol = solve(prob,Tsit5())#,callback=cb)
plot(sol,vars=([vOut,iL]))
