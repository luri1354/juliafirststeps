using ModelingToolkit
using DifferentialEquations

using Plots

@parameters t
@derivatives D'~t
#capacitor
@parameters C
@variables i(t) v(t)
eps_Capacitor= [D(v) ~ i/C]
capacitor1=ODESystem(eps_Capacitor,name=:capacitor1)


#switch
@parameters Tsw d Ron Roff Rsw
@variables i(t) vp(t) vn(t) Tsw

eps_SW= [ 0 ~ i - (vp-vn)/Rsw ]
sw1=ODESystem(eps_SS,name=:SW1)

function swOn!( integrator)
    integrator.p[7]= 0.0; RswH = Ron
    integrator.p[6] = 10e6; RswL = Roff
end
function time_swOn(t, u,integrator)
    Tsw = integrator.p[5]; Tsw
    Δt1 = Tsw*g
    return t < Δt1 ? Δt1 : t+Tsw
end
cbSWOn=IterativeCallback(time_swOn, swOn!)

function swOff!(integrator)
    integrator.p[7]=10e6; RswH = Roff;
    integrator.p[6] = 0.0; RswL = Ron;
end

function time_swOff(t, u, integrator)
    Tsw = integrator.p[5]; Tsw
    Δt2 = Tsw*(d+g)
    #@show integrator.t
    return t < Δt2 ? Δt2 : t+Tsw
end
cbSWOff=IterativeCallback(time_swOff, swOff!)



@variables t, Vs, R
@parameters i(t)

connections = [0 ~ Vs - R*i - capacitor1.v, 0~i-capacitor1.i]
connected = ODESystem(connections,t,[i],[Vs, R,Tsw],systems=[capacitor1])

#nlsys_func = generate_function(connected)

u0=[i=>0.0, capacitor1.i=>0.0, capacitor1.v=>0.0]
p=[Vs=>10.0, R=>2.0,capacitor1.C=>1.0
prob = ODEProblem(connected,u0,(0.0,10.0),p)
sol = solve(prob,Rosenbrock23(),force_dtmin=true)
#sol=solve(prob)
plot(sol,layout=(3,1))

using NLsolve

nl_f = @eval eval(nlsys_func[2])
# Make a closure over the parameters for for NLsolve.jl
f2 = (du,u) -> nl_f(du,u,(10.0,0.001,2))

result = nlsolve(f2,zeros(2))

plot(result)
