using ModelingToolkit
using DifferentialEquations
using Plots
using BenchmarkTools

mean_ms=zeros(4);

cd(raw"D:\Development\Julia\julia_DifferentialEquations")
display("Load simple switch example:")
include("test_callbacks_switch.jl")
@benchmark sol1 = solve(prob1,callback=cb_1)
mean_ms[1] = 2.35
sol1 = solve(prob1,callback=cb_1)
plot(sol1,layout=(2,1),tspan=(9, 10) ,title="Default Syntax with switch [10 , 0]*[1 , 0]")

display("Load simple @ode_def example:")
include("test_callbacks_odeDef.jl")
@benchmark sol2 = solve(prob2,callback=cb_2)
mean_ms[2] = 2.33
sol2 = solve(prob2,callback=cb_2)
plot(sol2,layout=(2,1),tspan=(9, 10),title="ode_def")

display("Load ModelingToolkit example simple:")
include("test_callbacks_SciML.jl")
@benchmark sol3 = solve(prob3,callback=cb_3)
mean_ms[3] = 0.824
sol3 = solve(prob3,callback=cb_3)
plot(sol3,layout=(2,1),tspan=(9, 10),vars=([iL,vOut]),title="SciML ModelingToolkit")

display("Load ModelingToolkit example with mass  Matrix:")
include("test_callbacks_SciML_massMatrix.jl")
@benchmark sol4 = solve(prob4,Rodas5(),callback=cb_4)
mean_ms[3] = 2.67
sol4 = solve(prob4,Rodas5(),callback=cb_4,reltol=1e-8,abstol=1e-8,dtmax=1e-3)
plot(sol4,layout=(3,1),tspan=(9, 10),vars=([iL,vSW,vL]),title="SciML ModelingToolkit Mass Matrix")
