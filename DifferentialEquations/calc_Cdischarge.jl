using DifferentialEquations
using Plots
#pyplot()

function c_discharge(du,u,p,t)
    du[1] = u[2]/p[1]
    du[2] = (p[4]-u[1])/p[2]
end

# Parameters
C=2.7E-9;
L=9.6E-6;
V_HS=400;
V_LS=210;
trr=65E-9;
p=[C, L, V_HS, V_LS, trr];
# initial condition for the system
I_LD= (V_LS-V_HS)*trr/L;
u0=[V_HS, I_LD];

tspan = (0.0,600.0E-9)
prob = ODEProblem(c_discharge,u0,tspan,p)
sol = solve(prob)

plot(sol,layout=(2,1))
